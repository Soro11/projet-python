import numpy as np
import random
import math 
import copy
import matplotlib.pyplot as plt


###Veuillez modifier les arguments ci dessous 
chaine ="MVHLTPEEKSAVTALWGKVNVD"
step = 100
####################


def seqHyd(seq):

	"""annote la séquence d'acide aminé dans une liste avec ses indices 
	seq -- chaîne de carcatères d'acide aminés
   
	"""
	list_h = []
	hydrophobe = ['A', 'V', 'F', 'P', 'L', 'I']
	for i in range(len(seq)):
		if seq[i] in hydrophobe: 
			list_h.append([seq[i], 'H', i])          ##on stock sa lettre, sa polarité et sa position dans la chaîne
		else:
			list_h.append([seq[i], 'P', i])
	return list_h

def init_matrice(list_h):

	"""initialise la matrice des séquences avec la sequénce d'acide aminés
	list_h -- liste d'acide aminés
   
	"""
	mat = np.empty((len(list_h)+2, len(list_h)+2),
		dtype = object)
	mid = int(len(mat)/2)                               ##calcul de l'indice de la matrice du milieu
	for i in range(len(list_h)):
		list_h[i].append(mid)                           ##on stocke les coordonnées des acids aminés dans la liste
		list_h[i].append(i+1)
		mat[mid, i+1] = list_h[i]	                    ##chaque cas e la matrice correspond à une liste d'informartions sur l'acide aminé
	return mat

def affiche_mat(mat): 

	"""affiche la matrice
	mat -- matrice numpy
   
	"""      
	for i in range(len(mat)):
		for j in range(len(mat)):
			if mat[i, j] == None:
				print ('-', end='')       
			else:
				print(mat[i, j][1], end='')
		print()

def calcul_ene(list_h, mat):

	"""affiche la matrice
	mat -- matrice numpy
	list_h -- liste de la chaîn d'acide aminés
	"""      
	ec = 0                                          ##on initialise l'énergie 
	for i in range(len(mat)):
		for j in range (len(mat)):
			if mat[i, j] !=None and mat[i, j][1] == 'H':           ## on repère l'acide aminé polaire dans la grille 
				if j<len(mat)-1 and  mat[i, j+1] != None and mat[i, j+1][1] == 'H' \
				 and  abs(mat[i, j+1][2]-mat[i, j][2])>1:              ##on regarde pour chacun de ses voisins s'il a un voisin hydrophobe adjacent non connecté
					ec-=1
				if j>0 and mat[i, j-1] != None and mat[i, j-1][1] == 'H'\
				 and abs(mat[i, j-1][2]-mat[i, j][2])>1:
					ec-=1
				if i>0 and mat[i-1, j] != None and mat[i-1, j][1] == 'H' \
				 and abs(mat[i-1, j][2]-mat[i, j][2])>1:
					ec-=1
				if i<len(mat)-1 and mat[i+1, j] != None and mat[i+1, j][1] == 'H'\
				 and abs(mat[i+1, j][2]-mat[i, j][2])>1:
					ec-=1 
	return ec/2						                                 ##on divise par 2, car la boucle compte deux fois pour la même énergie 																 	

def emplacement_libre_extr(mat, i, j, ind):
	"""recherche les emplacement libres 

	mat -- matrice numpy
	i,j -- entiers coordonnées de l'acide aminé 
	ind -- entiers position de l'indice dans la liste 
	"""      
	free = []
	if i<len(mat)-1 and mat[i+1, j] == None:
		free.append((i+1, j, ind))                                             ## on stocke les coordonnées libres et l'indice auquel l'acide aminé correspond
	if i>0 and mat[i-1, j] == None: 
		free.append((i-1, j, ind))
	if j>0 and mat[i, j-1] == None: 
		free.append((i, j-1, ind))
	if j<len(mat)-1 and mat[i, j+1] == None: 
		free.append((i, j+1, ind))
	return free

def emplacement_libre_coin(mat, i, j, ind):
	"""recherche les emplacement libres 
	mat -- matrice numpy
	i,j --  entiers coordonnées de l'acide aminé 
	ind -- entiers position de l'indice dans la liste  
	"""      
	free = []
	if j<len(mat)-1 and i< len(mat)-1 and  mat[i, (j+1)] != None and mat[(i+1), j] != None and mat[i+1, j+1] == None:	
		if abs(mat[(i+1), j][2] -  mat[i,j][2])==1 and abs(mat[i, (j+1)][2] - mat[i,j][2])==1:
			free.append((i+1, j+1, ind))

	if j>0 and i>0 and mat[i, (j-1)]!=None and mat[(i-1), j]!=None  and mat[i-1, j-1] == None:
		if abs(mat[i, (j-1)][2]-mat[i, j][2])==1 and abs(mat[(i-1), j][2]-mat[i, j][2]) == 1:
			free.append((i-1, j-1, ind))
	return free


def nb_voisin(i,j,mat):
	count=0
	if i<len(mat)-1 and mat[i+1, j] == None:
		count+=1                                               ## on stocke les coordonnées libres et l'indice auquel l'acide aminé correspond
	if i>0 and mat[i-1, j] == None: 
		count+=1
	if j>0 and mat[i, j-1] == None: 
		count+=1
	if j<len(mat)-1 and mat[i, j+1] == None: 
		count+=1
	return count


def emplacement_libre_vilebrequin(mat, i, j, ind):

	"""recherche les emplacement libres 
	mat -- matrice numpy
	i,j -- entiers coordonnées de l'acide aminé 
	ind -- entiers position de l'indice dans la liste 
	"""      
	free=[]
	if i>1 and i<len(mat)-2 and j>1 and j <len(mat)-2:
		if mat[i, j+1] != None and mat[i+1, j+1] != None and mat[i+1, j] != None and nb_voisin(i,j,mat)<=2 and nb_voisin(i,j+1,mat)<=2:
			if  abs(mat[i, j+1][2] -  mat[i,j][2])==1 and abs(mat[i+1, j][2] - mat[i+1,j+1][2])==1  and \
				 abs(mat[i+1,j][2] -  mat[i,j][2])==1 :                                                                          #u coin haut /gauche 																						 #u coin haut
				if mat[i, j+2] == None and mat[i+1, j+2] == None: 
					free.append((i, j+2, i+1, j+2,i+1,j,ind))

		if mat[i-1, j] != None and mat[i, j+1] != None and mat[i-1, j+1] != None and nb_voisin(i,j,mat)<=2 and nb_voisin(i-1,j,mat)<=2:
			if  abs(mat[i, j+1][2] -  mat[i,j][2])==1 and abs(mat[i-1, j][2] - mat[i-1,j+1][2])==1 and \
				abs(mat[i-1, j][2] -  mat[i,j][2])==1:
				if mat[i, j+2] == None and mat[i-1, j+2] == None:                                                                   #coin bas /gauche
					free.append((i, j+2, i-1, j+2,i-1,j, ind))

		if mat[i, j-1] != None and mat[i+1, j] != None and mat[i+1, j-1] != None and nb_voisin(i,j,mat)<=2 and nb_voisin(i+1,j,mat)<=2:
			if  abs(mat[i, j-1][2] -  mat[i,j][2])==1 and abs(mat[i+1, j][2] - mat[i+1,j-1][2])==1  and \
				abs(mat[i+1, j][2] -  mat[i,j][2])==1:
				if mat[i, j-2] == None and mat[i+1, j-2] == None:                                                                   #coin haut /droiy
					free.append((i, j-2, i+1, j-2,i+1,j, ind))	                  

		if mat[i, j-1] != None and mat[i-1, j] != None and mat[i-1, j-1] != None and nb_voisin(i,j,mat)<=2 and nb_voisin(i-1,j,mat)<=2:
			if  abs(mat[i, j-1][2] -  mat[i,j][2])==1 and abs(mat[i-1, j][2] - mat[i-1,j-1][2])==1 and \
				abs(mat[i-1,j][2] -  mat[i,j][2])==1 :
				if mat[i, j-2] == None and mat[i-1, j-2] == None: 
					free.append((i, j-2, i-1, j-2,i-1,j, ind))	
	return free

def emplacement_libre_mouvement_tire(mat, i, j,k,l,ind):
	"""recherche les emplacement libres 
	mat -- matrice numpy
	i,j -- entiers coordonnées de l'acide aminé 
	k,l -- entiers  coordonées de l acide amine ind+1
	ind- indice de l elememnt regarder (de list_h)
	"""
	free = []
	if i<len(mat)-1 and j<len(mat)-1 and  l<len(mat)-1  and i+1 == k and j+1 == l+1 and mat[i+1, j+1]==None and mat[i, j+1]==None: 
		free.append((i+1, j+1, i, j+1, ind))
	if i>0 and j>0 and  l>0  and i<len(mat) and i+1 == k and j-1 == l-1 and mat[i+1, j-1]==None and mat[i, j-1]==None:
		free.append((i+1, j-1, i, j-1, ind))
	return free


def deplacement(mat, list_h, list_free):

	"""fonction qui factorise les déplacments coin et extremités
	mat -- matrice numpy
	list_h -- liste d'acide aminés
	list_free-- liste des positions disponibles 
	"""  
	rand = random.randint(0, len(list_free)-1)
	irand = list_free[rand]
	ind = irand[2]
	mat[list_h[ind][3], list_h[ind][4]] = None
	mat[irand[0], irand[1]] = list_h[ind]
	list_h[ind][3] = irand[0]
	list_h[ind][4] = irand[1]

def mouvement_extremite(list_h, mat):

	"""fonction qui factorise les déplacments coin et extremités
	mat -- matrice numpy
	list_h -- liste d'acide aminés
	list_free-- liste des positions disponibles 
	"""  
	n = len(list_h)
	free = emplacement_libre_extr(mat, list_h[1][3], list_h[1][4], 0) + \
	 emplacement_libre_extr(mat, list_h[n-2][3], list_h[n-2][4], n-1)
	if len(free)>0:
		deplacement(mat, list_h, free)
		return True
	return False

def mouvement_coin(list_h, mat):

	"""fonction qui effectue un mouvement coin
	mat -- matrice numpy
	list_h -- liste d'acide aminés
	
	"""
	free = []
	for i in range(1, len(list_h)-1):
		free+=emplacement_libre_coin(mat, list_h[i][3],                   #recupere les emplacements libre
			list_h[i][4], i)
	if len(free)>0:
		deplacement(mat, list_h, free)
		return True 
	return False

def mouvement_vilebrequin(list_h, mat):
	"""fonction qui effectue un mouvement vilebrequin
	mat -- matrice numpy
	list_h -- liste d'acide aminés
	
	"""
	free = []
	for i in range(1, len(list_h)-1):
		free += emplacement_libre_vilebrequin(mat, list_h[i][3], 
			list_h[i][4], i)
	if len(free)>0:
		rand = random.randint(0, len(free)-1)
		ipr = free[rand]
		print (ipr)
		mat[ipr[0], ipr[1]] = list_h[ipr[6]]
		mat[list_h[ipr[6]][3], list_h[ipr[6]][4]] = None                #deplacement de l'indice selelectionné
		list_h[ipr[6]][3]=ipr[0]
		list_h[ipr[6]][4]=ipr[1]
		
		mat[ipr[2], ipr[3]] = mat[ipr[4], ipr[5]]                      #deplacement du coude 
		list_h[mat[ipr[4], ipr[5]][2]][3]=ipr[2]
		list_h[ mat[ipr[4], ipr[5]][2]][4]=ipr[3]
		mat[ipr[4], ipr[5]] = None
		return True 
	return False

def mouvement_tire(list_h, mat):
	"""fonction qui effectue un mouvement vilebrequin
	mat -- matrice numpy
	list_h -- liste d'acide aminés
	
	"""
	free = []
	for i in range(1, len(list_h)-1):
		free += emplacement_libre_mouvement_tire(mat, list_h[i][3], 
			list_h[i][4], list_h[i+1][3], list_h[i+1][4],i)
	if len(free)>0:
		rand = random.randint(0, len(free)-1)
		ind = free[rand][4]
		list_bis = copy.deepcopy(list_h)
		mat[free[rand][0], free[rand][1]] = list_h[ind]
		mat[list_h[ind][3], list_h[ind][4]] = None
		list_h[ind][3] = free[rand][0]
		list_h[ind][4] = free[rand][1]
		mat[free[rand][2], free[rand][3]] = list_h[ind-1]
		mat[list_h[ind-1][3], list_h[ind-1][4]] = None
		list_h[ind-1][3] = free[rand][2]
		list_h[ind-1][4] = free[rand][3]
		while mat[list_bis[ind][3], list_bis[ind][4]]==None and ind >=2: 
			mat[list_bis[ind][3], list_bis[ind][4]] = list_h[ind-2]
			mat[list_bis[ind-2][3], list_bis[ind-2][4]] = None
			list_h[ind-2][3] = list_bis[ind][3]
			list_h[ind-2][4] = list_bis[ind][4]
			ind = ind-1
		return True
	return False

def prob(ec1, ec2):
	"""fonction qui calcul la proba de changer de conformation
	ec1 -- float energie
	ec2 -- float energie
	
	"""
	delta = ec2-ec1
	prob = 0
	if delta <= 0: 
		return 1
	else: 
		q = random.uniform(0, 1)
		if q > math.exp(-delta):
			return 1
	return 0 

def mc_search(seq, step):
	"""fonction qui calcul la proba de changer de conformation
	seq -- chaine de caractères
	step --  nombre de pas 
	"""
	ene = []                               #liste qui contient les energies de conformation
	bool = 0                               #flag
	list_h = seqHyd(seq)                 #conversuin chaine en liste
	print(list_h[2])
	mat = init_matrice(list_h)
	affiche_mat(mat)
	mouvement_extremite(list_h, mat)
	affiche_mat(mat)
	ene.append(calcul_ene(list_h, mat))	
	for i in range(1, step):
		mat_precedent = copy.deepcopy(mat) 
		ec1 = calcul_ene(list_h, mat)
		while bool != True:                                    ##on tire un des mouvement au hasard jusq'à ce qu'il soit realisable
			r = random.randint(1,4)
			if r == 1:
				bool = mouvement_extremite(list_h, mat)
				print("extr")
			if r == 2: 
				bool = mouvement_coin(list_h, mat)
				print("coin")
			if r == 3: 
				bool = mouvement_vilebrequin(list_h, mat)
				print("vil")
			if r == 4: 
				bool = mouvement_tire(list_h, mat)
				print("tire")
		ec2 = calcul_ene(list_h, mat)                      #calcul de l'energie de la conformation obtenue
		print(ec2)
		p = prob(ec1, ec2)
		bool = 0                                           
		if p == 0: 
			mat = copy.deepcopy(mat_precedent) #si la probabilté ne permer pas de changer de conformation alors on revient à la conformation precedente
		ene.append(ec1)
		affiche_mat(mat)
	plt.plot(np.array(ene))
	plt.title ("Energie de la conformation de la sequence en fonction du temps")
	plt.ylabel("Energie")
	plt.xlabel("Temps")
	plt.show()	


mc_search(chaine,step) 





